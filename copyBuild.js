const fs = require('fs-extra');
const path = require('path');
const { exec } = require('child_process');

// Đường dẫn tới folder build
const buildPath = path.join(__dirname, '/build');

// Đường dẫn tới folder đích (dist, cũng là belischool-build)
const destPath = path.join(__dirname, '/belischool-build');

// Đường dẫn tới folder submodule
const submodulePath = path.join(__dirname, '/belischool-build');

// Hàm chạy lệnh git pull cho submodule
function gitPullSubmodule(callback) {
    exec('git pull', { cwd: submodulePath }, (err, stdout, stderr) => {
        if (err) {
            console.error('Có lỗi xảy ra khi chạy lệnh git pull:', err);
            return callback(err);
        }
        console.log('Git pull output:', stdout);
        if (stderr) {
            console.error('Git pull stderr:', stderr);
        }
        callback(null);
    });
}

// Hàm copy nội dung từ folder build tới folder đích
function copyBuildAndSubmodule(callback) {
    fs.copy(buildPath, destPath, err => {
        if (err) {
            console.error('Có lỗi xảy ra khi copy folder build:', err);
            return callback(err);
        }
        console.log('Copy folder build thành công!');
    });
}

// Thực hiện git pull submodule trước, rồi mới copy
gitPullSubmodule(err => {
    if (!err) {
        copyBuildAndSubmodule(err => {
            if (err) {
                console.error('Quá trình copy không thành công:', err);
            } else {
                console.log('Quá trình copy hoàn thành!');
            }
        });
    } else {
        console.error('Không thể hoàn thành quá trình do lỗi git pull:', err);
    }
});
