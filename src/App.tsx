import {Routes, Route, HashRouter} from 'react-router-dom';
import React from "react";
import HomePage from "./pages/HomePage";
import NotFoundPage from "./pages/NotFoundPage";

const App: React.FC = () => (
    <HashRouter>
        <Routes>
            <Route path="/" element={<HomePage />} />
            {/* Uncomment this line if you have a route like users/:id */}
            {/* <Route path="users/:id" element={<Users />} /> */}
            <Route path="*" element={<NotFoundPage />} />
        </Routes>
    </HashRouter>
);

export default App;
